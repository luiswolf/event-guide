# Guia de eventos

Aplicativo desenvolvido em teste para vaga de desenvolvedor iOS entre os dias 27/09/2019 e 29/09/2019.

> **OBJETIVO**
>
> * Criar um aplicativo em Swift que consuma uma REST API e exiba uma listagem de eventos
> * Cada item da lista deve permitir acesso à detalhes do evento
> * No detalhe do evento é importante exibir suas informações e opções de check-in e compartilhamento
>

O aplicativo foi desenvolvido procurando oferecer uma lógica para a aplicação/usuário considerando os requisitos do teste.

Escolhi como versão mínima a versão do iOS 10 mas não tem nenhuma biblioteca específica para esta versão, portanto o aplicativo pode ser alterado para versões anteriores tranquilamente.

Para esse tipo de aplicativo, que possui uma visualização principal mostrando uma lista de itens e uma visualização detalhada de cada item, eu costumava escolher o template do Master-Detail do Xcode, porque é mais rápido configurar o projeto e traz o UISplitViewController que mostra visualização aperfeiçoada no iPad, porém decidi criar um projeto com o template Single View Application e implementar o UISplitViewController manuelamente, pois acredito que mantém o código mais limpo e permite fácil extensão do projeto.

Usei o padrão RootViewController pois pode-se implementar outros conceitos sem a necessidade de uma grande reestruturação no projeto.

## Requisitos

* iOS 10.0+
* Xcode 10.3+
* Swift 5.0+

## Dependências

Para gerenciamento de dependências, utilizei o CocoaPods com as bibliotecas abaixo (todas em versões estáveis):

* [Alamofire] - Biblioteca que realiza chamadas de serviços para APIs Rest.
* [AlamofireObjectMapper] - Biblioteca faz utilizada a biblioteca ObjectMapper para converter responstas JSON em objetos Swift automaticamente.
* [ObjectMapper] - Biblioteca que converte (parse) responstas JSON em objetos Swift.
* [Segmentio] - Biblioteca que personaliza o componente nativo UISegmentedControl.
* [Toaster] - Biblioteca auxiliar que exibe mensagens informativas na aplicação.

[Alamofire]: <https://github.com/Alamofire/Alamofire>
[AlamofireObjectMapper]: <https://github.com/tristanhimmelman/AlamofireObjectMapper>
[ObjectMapper]: <https://github.com/tristanhimmelman/ObjectMapper>
[Segmentio]: <https://github.com/Yalantis/Segmentio>
[Toaster]: <https://github.com/devxoul/Toaster>

## Apresentação da aplicação

### Perfil

Quando executado pela primeira vez, o aplicativo apresenta esta tela para que o usuário informe seus dados pessoais.
Nela consta um formulário solicitando o dados do usuário (nome e e-mail apenas) para que o mesmo possa confirmar presença em um evento e manter a lógica da aplicação.

Em nenhum momento o usuário é obrigado a informar estes dados, e o mesmo pode Pular este procedimento (na primeira vez que esta tela é exibida) ou fechar quando o mesmo acessa a tela em outro momento.

<img src="screenshots/profile-01.png" width="300"><img src="screenshots/profile-02.png" width="300">
*** 

Ao preencher o formulário, caso exista alguma inconsistências nos dados, são exibidos mensagens informativas para o usuário.

<img src="screenshots/profile-03.png" width="300"><img src="screenshots/profile-04.png" width="300"><img src="screenshots/profile-05.png" width="300"><img src="screenshots/profile-06.png" width="300">

***

Caso o preenchimento seja realizado com sucesso, os dados informados são registrados no UserDefaults apenas, pois não foi exigido realizar nenhuma persistência externa e a escolha de utilizar UserDefaults foi feita pela simplicidade. 

Quando o usuário já está persistido no armazenamento local, é exibida a opção para remover os dados deste, apenas para poder simular os cenários executados novamente sem ter que excluir/instalar a aplicação. 

<img src="screenshots/profile-07.png" width="300"><img src="screenshots/profile-08.png" width="300">

### Lista de eventos

A lista de eventos mostra todos eventos retornados pela API, exibindo informações básicas de cada evento, como Título, Data, Banner e Preço.


<img src="screenshots/event-list-01.png" width="300">


Caso a requisição para a API retorne algum erro ou uma lista vazia, são exibidas as telas abaixo.

<img src="screenshots/event-list-04.png" width="300"><img src="screenshots/event-list-03.png" width="300">

### Detalhe do evento

A tela de detalhe do evento exibe o banner do evento (caso exista), título, data e opção para realizar o compartilhamento do evento.
Também são exibidas duas abas com dados do evento: Informações e Local.

<img src="screenshots/event-detail-01.png" width="300"><img src="screenshots/event-detail-02.png" width="300"><img src="screenshots/event-detail-03.png" width="300">

Caso o conteúdo da tela seja muito grande e seja necessário fazer scroll, a barra contendo os menus e opção para confirmar presença no evento é fixada ao topo da tela.

<img src="screenshots/event-detail-08.png" width="300">

***

O usuário pode confirmar presença no evento através do botão Confirmar presença. Para confirmação, o usuário deve estar cadastrado no aplicativo. Caso o mesmo não tenha preenchido seu perfil, a tela de Perfil será exibida solicitando os mesmos, e, assim que os dados forem informados, a confirmação de presença é realizada.
Este registro é guardado apenas enquanto o usuário está na tela de detalhe do evento pois o serviço de registro não faz a devida persistência no servidor.
Para simular o comportamento de confirmação/desconfirmação, foi feito apenas um teste local e a cada ação realizada é exibida mensagem informativa para o usuário.

<img src="screenshots/event-detail-06.png" width="300"><img src="screenshots/event-detail-05.png" width="300">

Em caso de erro na confirmação de presença, é exibido também mensagem informativa.

<img src="screenshots/event-detail-04.png" width="300">

***

Na aba Local, o usuário pode abrir a localização do evento no aplicativo de Mapa do dispositivo através do botão presente ao lado do endereço do evento.

<img src="screenshots/event-detail-02.png" width="300"><img src="screenshots/event-detail-07.png" width="300">

### iPad

A aplicação também é adaptada para o iPad que exibe a lista de eventos e o detalhe do evento no mesmo plano através do uso do UISplitViewController.

<img src="screenshots/ipad-01.png" width="450">