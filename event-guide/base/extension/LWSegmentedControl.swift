//
//  LWSegmentedControl.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class LWSegmentedControl: UISegmentedControl {
    
    private func configure() {
        let attributesSelected = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        let attributesNormal: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: UIFont.display,
            NSAttributedString.Key.foregroundColor: UIColor.red
        ]
        setTitleTextAttributes(attributesNormal, for: .normal)
        setTitleTextAttributes(attributesSelected, for: .selected)
        tintColor = UIColor.clear
        tintAdjustmentMode = .normal
        
        change()
        addChangeTarget()
    }
    
    func addChangeTarget() {
        addTarget(self, action: #selector(change), for: UIControl.Event.valueChanged)
    }
    
    @objc func change() {
        if let title = titleForSegment(at: selectedSegmentIndex) {
            for option in subviews {
                option.layer.cornerRadius = 5.0
                var optionLabel: UILabel?
                for sub in option.subviews {
                    if let label = sub as? UILabel {
                        optionLabel = label
                    }
                }
                if let optionLabel = optionLabel {
                    let color = UIColor.red
                    if optionLabel.text! == title {
                        option.backgroundColor = color
                    } else {
                        option.backgroundColor = .clear
                        optionLabel.textColor = color
                    }
                }
            }
        }
    }
    
    private func reset() {
        if allTargets.count > 0 {
            removeTarget(self, action: #selector(change), for: UIControl.Event.valueChanged)
        }
        removeAllSegments()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        configure()
    }
}
