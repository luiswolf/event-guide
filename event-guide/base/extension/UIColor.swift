//
//  UIColor.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

extension UIColor {
    static let blue = #colorLiteral(red: 0.007843137255, green: 0.2862745098, blue: 0.3647058824, alpha: 1)
    static let lightBlue = #colorLiteral(red: 0.662745098, green: 0.831372549, blue: 0.9058823529, alpha: 1)
    static let lightestBlue = #colorLiteral(red: 0.8901960784, green: 0.9568627451, blue: 0.9843137255, alpha: 1)
    static let green = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.1098039216, alpha: 1)
    static let lightGreen = #colorLiteral(red: 0.7568627451, green: 0.7647058824, blue: 0.3176470588, alpha: 1)
    static let lightestGreen = #colorLiteral(red: 0.8666666667, green: 0.8705882353, blue: 0.5607843137, alpha: 0.7998180651)
    static let orange = #colorLiteral(red: 0.8274509804, green: 0.5921568627, blue: 0.2549019608, alpha: 1)
    static let yellow = #colorLiteral(red: 0.8941176471, green: 0.7882352941, blue: 0.4666666667, alpha: 1)
    static let beige = #colorLiteral(red: 0.968627451, green: 0.9568627451, blue: 0.8980392157, alpha: 1)
    static let lightGray = #colorLiteral(red: 0.9294117647, green: 0.937254902, blue: 0.9254901961, alpha: 1)
    static let lightestGray = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
}

