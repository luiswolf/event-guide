//
//  LWNavigationController.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 27/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class LWNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }
}

extension LWNavigationController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}
