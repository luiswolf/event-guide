//
//  Double.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

extension Double {
    func currencyString(localeIdentifier: String = "pt_BR") -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.locale  = Locale.init(identifier: localeIdentifier)
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        currencyFormatter.currencyCode = (Locale.current as NSLocale).displayName(forKey: NSLocale.Key.currencySymbol, value: NSLocale.Key.currencyCode)
        return currencyFormatter.string(from: NSNumber(value: self))!
    }
}
