//
//  CLLocationCoordinate2D.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import CoreLocation

extension CLLocationCoordinate2D : CustomStringConvertible {
    
    public var description : String {
        return "\(self.latitude);\(self.longitude)"
    }
    
    typealias LocationCallback = (AddressModel?) -> Void
    
    func getLocation(callback: @escaping LocationCallback) {
        if let address = CacheHelper.sharedInstance.getLocation(forKey: self.description) {
            callback(address)
            return
        }
        
        let ceo: CLGeocoder = CLGeocoder()
        let loc: CLLocation = CLLocation(latitude:self.latitude, longitude: self.longitude)
        
        ceo.reverseGeocodeLocation(loc) { (placemarks, error) in
            guard error == nil else {
                callback(nil)
                return
            }
            guard let pm = placemarks, !pm.isEmpty, let placemark = pm.first else {
                callback(nil)
                return
            }
            
            let address = AddressModel()
            address.state = placemark.administrativeArea
            address.country = placemark.country
            address.neighborhood = placemark.subLocality
            address.city = placemark.locality
            address.zipCode = placemark.postalCode
            address.street = placemark.thoroughfare
            address.number = placemark.subThoroughfare
            
            CacheHelper.sharedInstance.storeLocation(address, forKey: self.description)
            callback(address)
        }
    }
}
