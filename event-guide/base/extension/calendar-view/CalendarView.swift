//
//  CalendarView.swift
//  EventGuideApp
//
//  Created by Luís Wolf on 15/11/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

@IBDesignable
class CalendarView: UIView {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBInspectable var date: Date! = Date() {
        didSet {
            dayLabel?.text = DateHelper.sharedInstance.getDay(from: date)
            monthLabel?.text = DateHelper.sharedInstance.getMonth(from: date)
        }
    }
    
    private var contentView : UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func xibSetup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
}

