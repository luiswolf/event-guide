//
//  LWSeparatorView.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

@IBDesignable
class LWSeparatorView: UIView {
    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = UIColor.lightGray
        let divisor = UIScreen.main.scale > 2 ? 2 : UIScreen.main.scale
        heightConstraint.constant = 1.0 / divisor
    }
}
