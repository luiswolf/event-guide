//
//  LWLabel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

@IBDesignable
class LWLabel: UILabel {

    fileprivate var fontName: UIFont = UIFont.regular
    
    @IBInspectable
    var fontColor: UIColor = UIColor.blue {
        didSet {
            textColor = fontColor
        }
    }
    
    @IBInspectable
    var isBold: Bool = false {
        didSet {
            fontName = isBold ? UIFont.bold : UIFont.regular
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        textColor = fontColor
        font = fontName.withSize(font.pointSize)
    }

}
