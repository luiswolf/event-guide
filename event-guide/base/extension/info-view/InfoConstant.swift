//
//  InfoConstant.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

/// Constants for the Info package
enum InfoConstant {
    
    /// List of titles used in Info package
    enum Title {
        static let noData = "Ops"
        static let notice = "Aviso"
        static let noConnection = "Sem conexão"
    }
    
    /// List of images used in Info package
    enum Image {
        static let noData = "il-empty"
        static let error = "il-error"
    }
    
    /// List of messages used in Info package
    enum Message {
        static let noConnection = "Você está offline. Por favor verifique sua conexão com a internet."
        static let error = "Ocorreu um erro inesperado. Por favor, tente novamente mais tarde."
        
    }
}
