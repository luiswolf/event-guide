//
//  ErrorView.swift
//  EventGuideApp
//
//  Created by Luís Wolf on 18/11/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

@IBDesignable
class InfoView: UIView {
    
    var delegate: InfoViewDelegate?
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBInspectable var message: String! = InfoConstant.Message.error {
        didSet {
            messageLabel?.text = message
        }
    }
    @IBInspectable var title: String! = InfoConstant.Title.notice {
        didSet {
            titleLabel?.text = title
        }
    }
    @IBOutlet weak var actionButton: UIButton! {
        didSet {
            actionButton?.titleLabel?.font = UIFont.regular.withSize(16.0)
        }
    }
    var isOffline: Bool = false {
        didSet {
            if isOffline {
                iconImageView?.image = UIImage(named: InfoConstant.Image.error)
                titleLabel?.text = InfoConstant.Title.noConnection
                message = InfoConstant.Message.noConnection
            } else {
                iconImageView?.image = UIImage(named: InfoConstant.Image.error)
                titleLabel?.text = InfoConstant.Title.notice
                message = InfoConstant.Message.error
            }
        }
    }
    
    private var contentView : UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func xibSetup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
}

extension InfoView {
    @IBAction func didPressRetryButton(_ sender: UIButton) {
        delegate?.didPressRetryButton()
    }
}
