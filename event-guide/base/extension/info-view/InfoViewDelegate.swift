//
//  InfoViewDelegate.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

protocol InfoViewDelegate {
    func didPressRetryButton()
}
