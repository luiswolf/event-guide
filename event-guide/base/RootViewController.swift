//
//  RootViewController.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 27/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    private var current: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaultsHelper.Application.isUsed() {
            change(viewController: EventListTableViewController.prepareSplitViewController())
        } else {
            UserDefaultsHelper.Application.registerIsUsed()
            let profile = ProfileTableViewController.prepareNavigationController()
            if let vc = profile?.topViewController as? ProfileTableViewController {
                vc.delegate = self
                vc.isModal = false
            }
            change(viewController: profile)
        }
    }
    
    func change(viewController vc: UIViewController?) {
        guard let vc = vc else { return }
        
        if let vc = vc as? UISplitViewController {
            let navigationController = vc.viewControllers[vc.viewControllers.count-1] as! UINavigationController
            navigationController.topViewController!.navigationItem.leftBarButtonItem = vc.displayModeButtonItem
            vc.delegate = self
        }
        
        addChild(vc)
        vc.view.frame = view.bounds
        view.addSubview(vc.view)
        vc.didMove(toParent: self)
        
        current?.willMove(toParent: nil)
        current?.view.removeFromSuperview()
        current?.removeFromParent()
        current = vc
    }
}

extension RootViewController: ProfileDelegate {
    func didFinishRegister() {
        change(viewController: EventListTableViewController.prepareSplitViewController())
    }
    func didSkipRegister() {
        change(viewController: EventListTableViewController.prepareSplitViewController())
    }
}

// MARK: - UISplitViewController Delegate
extension RootViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? LWSplitViewControllerDetailProtocol else { return false }
        if !topAsDetailController.hasSelectedItem() {
            return true
        }
        return false
    }
    func splitViewController(_ splitViewController: UISplitViewController, showDetail vc: UIViewController, sender: Any?) -> Bool {
        guard splitViewController.isCollapsed, let navController = vc as? UINavigationController, let detailVC = navController.topViewController else {
            return false
        }
        splitViewController.showDetailViewController(detailVC, sender: sender)
        return true
    }
}
