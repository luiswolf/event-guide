//
//  LWStateProtocol.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

protocol LWStateProtocol {
    func isOnline() -> Bool
    func isSuccess() -> Bool
    func isError() -> Bool
}
