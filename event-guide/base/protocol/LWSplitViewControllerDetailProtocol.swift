//
//  LWSplitViewControllerDetailProtocol.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 27/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

protocol LWSplitViewControllerDetailProtocol {
    func hasSelectedItem() -> Bool
}
