//
//  LocationTransform.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

open class LocationTransform: TransformType {
    public typealias Object = CLLocationDegrees
    public typealias JSON = CLLocationDegrees
    
    open func transformFromJSON(_ value: Any?) -> CLLocationDegrees? {
        if let value = value as? CLLocationDegrees { return value }
        if let value = value as? String { return CLLocationDegrees(value) }
        return nil
    }
    
    open func transformToJSON(_ value: CLLocationDegrees?) -> CLLocationDegrees? {
        return value
    }
}
