//
//  StringIntegerTransform.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper

open class StringIntegerTransform: TransformType {
    public typealias Object = Int
    public typealias JSON = String
    
    open func transformFromJSON(_ value: Any?) -> Int? {
        if let value = value as? Int { return value }
        if let value = value as? String { return Int(value) }
        return nil
    }
    
    open func transformToJSON(_ value: Int?) -> String? {
        guard let value = value else { return nil }
        return String(value)
    }
}
