//
//  TimeIntervalDateTransform.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper

open class TimeIntervalDateTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = TimeInterval
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        guard let timeInterval = value as? TimeInterval else { return nil }
        return Date(timeIntervalSince1970: timeInterval / 1000)
    }
    
    open func transformToJSON(_ value: Date?) -> TimeInterval? {
        guard let value = value else { return nil }
        return value.timeIntervalSince1970 * 1000
    }
}
