//
//  AppDelegate.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 27/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = RootViewController()
        window?.makeKeyAndVisible()
        AppDelegate.configureMainAppearance()
        return true
    }
}

// MARK: - Helper
extension AppDelegate {
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    var rootViewController: RootViewController {
        return window!.rootViewController as! RootViewController
    }
}

extension AppDelegate {
    fileprivate static func configureMainAppearance() {
        UINavigationBar.appearance().barTintColor = UIColor.white.withAlphaComponent(0.5)
        UINavigationBar.appearance().tintColor = UIColor.green
        UINavigationBar.appearance().titleTextAttributes = [
            .foregroundColor : UIColor.green
        ]
        UISegmentedControl.appearance().tintColor = UIColor.green
        UIButton.appearance().tintColor = UIColor.green
    }
}

