//
//  ResponseDTO.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

class ResponseDTO<T> {
    let success: Bool
    let message: String?
    let data: T?
    
    init(success: Bool, message: String?) {
        self.success = success
        self.message = message
        self.data = nil
    }
    
    init(success: Bool, data: T? = nil) {
        self.success = success
        self.data = data
        self.message = nil
    }
}
