//
//  AddressModel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import CoreLocation

class AddressModel {
    var street: String?
    var country: String?
    var state: String?
    var city: String?
    var neighborhood: String?
    var zipCode: String?
    var number: String?
    
    func getFormattedString() -> String? {
        var formattedString: String?
        if let street = street {
            formattedString = street
            if let number = number {
                formattedString = (formattedString ?? "") + ", " + number
            }
        }
        if let neighborhood = neighborhood {
            formattedString = (formattedString ?? "") + " - " + neighborhood
        }
        if let zipCode = zipCode {
            formattedString = (formattedString ?? "") + ", " + zipCode
        }
        if let city = city {
            formattedString = (formattedString ?? "") + " - " + city
            if let state = state {
                formattedString = (formattedString ?? "") + ", " + state
            }
        }
        return formattedString
    }
}
