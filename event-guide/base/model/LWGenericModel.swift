//
//  LWGenericModel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper

class LWGenericModel<T: Mappable> {
    var object: T?
    var array: [T]?
    
    init(object: T) {
        self.object = object
    }
    init(array: [T]) {
        self.array = array
    }
}
