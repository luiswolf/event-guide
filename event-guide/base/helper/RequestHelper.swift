//
//  RequestHelper.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

final class RequestHelper {
    private let imageTypes = ["image/png", "image/jpeg"]
    
    func getObject<T>(ofType: T.Type, fromUrl url: String, andReturnTo callback: @escaping (ResponseDTO<T>) -> Void) where T: Mappable {
        
        Alamofire.request(url).responseObject { (response: DataResponse<T>) in
            switch response.result {
                case .success(let data):
                    let responseDTO = ResponseDTO<T>(success: true, data: data)
                    DispatchQueue.main.async {
                        callback(responseDTO)
                    }
                case .failure(let error):
                    let responseDTO = ResponseDTO<T>(success: false, message: error.localizedDescription)
                    DispatchQueue.main.async {
                        callback(responseDTO)
                    }
            }
        }
    }
    
    func getArray<T>(ofType: T.Type, fromUrl url: String, andReturnTo callback: @escaping (ResponseDTO<[T]>) -> Void) where T: Mappable {
        
        Alamofire.request(url).responseArray { (response: DataResponse<[T]>) in
            switch response.result {
            case .success(let data):
                let responseDTO = ResponseDTO<[T]>(success: true, data: data)
                DispatchQueue.main.async {
                    callback(responseDTO)
                }
            case .failure(let error):
                let responseDTO = ResponseDTO<[T]>(success: false, message: error.localizedDescription)
                DispatchQueue.main.async {
                    callback(responseDTO)
                }
            }
        }
    }
    
    func post<T>(object: T, toUrl url: String, andReturnTo callback: @escaping (ResponseDTO<T>) -> Void) where T: Mappable {
        
        Alamofire.request(url, method: HTTPMethod.post, parameters: object.toJSON(), encoding: JSONEncoding.default).responseObject { (response: DataResponse<T>) in
            switch response.result {
            case .success(let data):
                let responseDTO = ResponseDTO<T>(success: true, data: data)
                DispatchQueue.main.async {
                    callback(responseDTO)
                }
            case .failure(let error):
                let responseDTO = ResponseDTO<T>(success: false, message: error.localizedDescription)
                DispatchQueue.main.async {
                    callback(responseDTO)
                }
            }
        }
    }

    func download(fromUrl url: String, andReturnTo callback: @escaping (ResponseDTO<Data>) -> Void) {

        Alamofire.request(url)
            .validate(contentType: self.imageTypes)
            .responseData { response in
                
            switch response.result {
                case .success:
                    let responseDTO = ResponseDTO<Data>(success: true, data: response.data)
                    DispatchQueue.main.async {
                        callback(responseDTO)
                    }
                case .failure(let error):
                    let responseDTO = ResponseDTO<Data>(success: false, message: error.localizedDescription)
                    DispatchQueue.main.async {
                        callback(responseDTO)
                    }
            }
        }
    }
}
