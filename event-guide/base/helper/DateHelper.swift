//
//  DateHelper.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

class DateHelper {
    let formatter = DateFormatter()
    static let sharedInstance = DateHelper()
    
    private init() {
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale.current
        if let locale = Locale.preferredLanguages.first {
            formatter.locale = Locale(identifier: locale)
        }
    }
    
    func getFormattedFullDate(from date: Date) -> String {
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        return formatter.string(from: date)
    }
    func getFormattedDate(from date: Date) -> String {
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter.string(from: date)
    }
    func getDay(from date: Date) -> String {
        formatter.dateFormat = "dd"
        return formatter.string(from: date)
    }
    func getMonth(from date: Date) -> String {
        formatter.dateFormat = "MMM"
        return formatter.string(from: date).uppercased()
    }
    func getHour(from date: Date) -> String {
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: date)
    }
}
