//
//  UserDefaultsHelper.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

struct UserDefaultsHelper {
    
    struct Application {
        static let isUsedKey = "com.luiswolf.applicationIsUsed"
        static func registerIsUsed(){
            UserDefaults.standard.set(true, forKey: isUsedKey)
        }
        static func isUsed() -> Bool {
            return UserDefaults.standard.bool(forKey: isUsedKey)
        }
        static func clear(){
            UserDefaults.standard.removeObject(forKey: isUsedKey)
        }
    }
    
    struct User {
        static let (nameKey, emailKey) = ("name", "email")
        static let userSessionKey = "com.luiswolf.user"
    
        static func save(user: PersonModel) {
            guard let email = user.email else { return }
            UserDefaults.standard.set([nameKey: user.name, emailKey: email], forKey: userSessionKey)
        }
        
        static func getUser()-> PersonModel? {
            guard let userJson = UserDefaults.standard.value(forKey: userSessionKey) as? [String: String] else {
                return nil
            }
            return PersonModel(userJson)
        }
        
        static func clearUserData(){
            UserDefaults.standard.removeObject(forKey: userSessionKey)
        }
    }
}
