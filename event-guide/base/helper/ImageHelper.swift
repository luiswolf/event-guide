//
//  ImageHelper.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class ImageHelper {
    var requestHelper = RequestHelper()
    
    func getImage(withPath path: String, withCompletion completion: @escaping (UIImage?)->Void) {
        if let image = CacheHelper.sharedInstance.getImage(forKey: path) {
            completion(image)
        } else {
            requestHelper.download(fromUrl: path) { response in
                if response.success, let data = response.data, let image = UIImage(data: data) {
                    CacheHelper.sharedInstance.storeImage(image, forKey: path)
                    completion(image)
                } else {
                    completion(nil)
                }
            }
        }
    }
}
