//
//  MessageHelper.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import Toaster

class MessageHelper {
    private var toast: Toast?
    static let sharedInstance = MessageHelper()
    
    private init() {
        ToastView.appearance().cornerRadius = 5.0
        ToastView.appearance().textInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
        ToastView.appearance().font = UIFont.regular
    }
    
    func show(message: String) {
        toast?.cancel()
        toast = Toast(text: message, duration: Delay.short)
        toast?.show()
    }
}
