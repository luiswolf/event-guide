//
//  CacheHelper.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class CacheHelper {
    private let location = NSCache<NSString, AddressModel>()
    private let image = NSCache<NSString, UIImage>()
    
    static let sharedInstance = CacheHelper()
    private init() {}
}

// MARK: - Location
extension CacheHelper {
    func getLocation(forKey key: String) -> AddressModel? {
        return location.object(forKey: NSString(string: key))
    }
    func storeLocation(_ address: AddressModel, forKey key: String) {
        guard getLocation(forKey: key) == nil else { return }
        self.location.setObject(address, forKey: NSString(string: key))
    }
}


// MARK: - Image
extension CacheHelper {
    func getImage(forKey key: String) -> UIImage? {
        return image.object(forKey: NSString(string: key))
    }
    func storeImage(_ image: UIImage, forKey key: String) {
        guard getImage(forKey: key) == nil else { return }
        self.image.setObject(image, forKey: NSString(string: key))
    }
}
