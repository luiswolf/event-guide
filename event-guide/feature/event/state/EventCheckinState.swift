//
//  EventCheckinState.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

enum EventCheckinState: LWStateProtocol {
    case error(message: String)
    case noConnection(message: String)
    case success(data: EventCheckinModel)
    
    func isOnline() -> Bool {
        guard case .noConnection = self else { return true }
        return false
    }
    func isSuccess() -> Bool {
        guard case .success = self else { return false }
        return true
    }
    func isError() -> Bool {
        switch self {
        case .noConnection, .error:
            return true
        default:
            return false
        }
    }
    func getData() -> EventCheckinModel? {
        guard case let .success(data: data) = self else { return nil }
        return data
    }
}
