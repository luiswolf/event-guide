//
//  EventCheckinModel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper

class EventCheckinModel: Mappable {
    var eventId: Int = 0
    var personId: Int = 0
    var name: String = String.init()
    var email: String = String.init()
    
    init() {}
    required init?(map: Map){}
    
    func mapping(map: Map) {
        personId <- (map["id"], StringIntegerTransform())
        eventId <- (map["eventId"], StringIntegerTransform())
        name <- map["name"]
        email <- map["email"]
    }
}
