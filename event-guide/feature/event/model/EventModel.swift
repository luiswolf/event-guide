//
//  EventModel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

class EventModel: Mappable {
    var id: Int = 0
    var date: Date = Date()
    var title: String = String.init()
    var description: String = String.init()
    var price: Double = 0.0
    var imageUrl: String?
    var people: [PersonModel]?
    
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- (map["id"], StringIntegerTransform())
        date <- (map["date"], TimeIntervalDateTransform())
        title <- map["title"]
        description <- map["description"]
        price <- map["price"]
        imageUrl <- map["image"]
        people <- map["people"]
        latitude <- (map["latitude"], LocationTransform())
        longitude <- (map["longitude"], LocationTransform())
    }
    
    func getCoordinate() -> CLLocationCoordinate2D? {
        guard let latitude = latitude, let longitude = longitude else { return nil }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
