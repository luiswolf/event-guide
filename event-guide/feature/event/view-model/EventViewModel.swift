//
//  EventViewModel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

class EventViewModel: NSObject {
    
    weak var delegate: LWResponseRequestProtocol?
    
    fileprivate var service: EventService
    fileprivate(set) var state: LWStateProtocol? {
        didSet {
            delegate?.didFinishRequest()
        }
    }
    
    init(service: EventService = EventService()) {
        self.service = service
    }
    
    func getList() {
//        state = EventListState.noData
//        state = EventListState.noConnection(message: "Sem conexão")
//        state = EventListState.error(message: "Erro de serviço")
//        return;
        
        service.getList { [weak self] response in
            guard let sself = self else { return }
            
            guard response.success, let data = response.data else {
                sself.state = EventListState.error(message: response.message ?? String.init())
                return
            }
            
            guard !data.isEmpty else {
                sself.state = EventListState.noData
                return
            }
            sself.state = EventListState.success(data: data)
        }
    }
    
    func getDetail(withId id: Int) {
//        state = EventDetailState.noConnection(message: "Sem conexão")
//        state = EventDetailState.error(message: "Erro de serviço")
//        return;
        service.getDetail(withId: id) { [weak self] response in
            guard let sself = self else { return }
            
            guard response.success, let data = response.data else {
                sself.state = EventDetailState.error(message: response.message ?? String.init())
                return
            }
            sself.state = EventDetailState.success(data: data)
        }
    }
    
    func checkin(atEventId id: Int) {
        guard let user = UserDefaultsHelper.User.getUser() else {
            state = EventCheckinState.error(message: EventConstant.Message.userError)
            return;
        }
        
        let obj = EventCheckinModel()
        obj.eventId = id
        obj.personId = user.id
        obj.name = user.name
        obj.email = user.email ?? String.init()
        
//        state = EventCheckinState.noConnection(message: "Sem conexão")
//        state = EventCheckinState.error(message: "Erro de serviço")
//        state = EventCheckinState.success(data: obj)
//        return;
        
        service.checkin(withModel: obj) { [weak self] response in
            guard let sself = self else { return }
            
            guard response.success, let _ = response.data else {
                sself.state = EventCheckinState.error(message: response.message ?? String.init())
                return
            }
            sself.state = EventCheckinState.success(data: obj)
        }
    }
}
