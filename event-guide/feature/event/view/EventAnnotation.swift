//
//  EventAnnotation.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import MapKit

class EventAnnotation: NSObject, MKAnnotation {
    var title: String? { return event.title }
    var event: EventModel!
    var coordinate: CLLocationCoordinate2D
    
    init(event: EventModel) {
        self.event = event
        self.coordinate = event.getCoordinate() ?? CLLocationCoordinate2D()
        super.init()
    }
}
