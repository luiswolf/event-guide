//
//  EventTableViewCell.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    fileprivate let formatter = DateHelper.sharedInstance
    fileprivate let imageHelper = ImageHelper()
    
    @IBOutlet fileprivate weak var bannerImageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var dateLabel: UILabel!
    @IBOutlet fileprivate weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerImageView?.layer.cornerRadius = 5.0
        bannerImageView?.layer.masksToBounds = true
        bannerImageView?.layer.borderColor = UIColor.lightGray.cgColor
        bannerImageView?.layer.borderWidth = 1.0
        accessoryType = .disclosureIndicator
        separatorInset = UIEdgeInsets(top: 0.0, left: 80.0, bottom: 0.0, right: 0.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ event: EventModel) {
        titleLabel.text = event.title
        dateLabel.text = formatter.getFormattedDate(from: event.date)
        var price = EventConstant.Label.free
        if event.price > 0 {
            price = event.price.currencyString()
        }
        priceLabel.text = price
        
        bannerImageView.image = UIImage(named: EventConstant.Image.defaultEvent)
        if let imageUrl = event.imageUrl {
            imageHelper.getImage(withPath: imageUrl) { [weak self] image in
                guard let sself = self, let image = image else { return }
                sself.bannerImageView.image = image
            }
        }
    }

}
