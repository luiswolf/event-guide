//
//  EventService.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

class EventService: NSObject {
    
    let requestHelper = RequestHelper()
    
    func getList(andReturnTo callback: @escaping ((ResponseDTO<[EventModel]>) -> Void)) {
        let url = EventConstant.Endpoint.event().getUrl()
        requestHelper.getArray(ofType: EventModel.self, fromUrl: url, andReturnTo: callback)
    }
    
    func getDetail(withId id: Int, andReturnTo callback: @escaping ((ResponseDTO<EventModel>) -> Void)) {
        let url = EventConstant.Endpoint.event(id: id).getUrl()
        requestHelper.getObject(ofType: EventModel.self, fromUrl: url, andReturnTo: callback)
    }
    
    func checkin(withModel model: EventCheckinModel, andReturnTo callback: @escaping ((ResponseDTO<EventCheckinModel>) -> Void)) {
        let checkin = EventConstant.Endpoint.checkin.getUrl()
        requestHelper.post(object: model, toUrl: checkin, andReturnTo: callback)
    }
    
}
