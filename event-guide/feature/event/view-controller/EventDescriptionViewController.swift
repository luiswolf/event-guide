//
//  EventDescriptionViewController.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class EventDescriptionTableViewController: UITableViewController {
    
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!
    @IBOutlet fileprivate weak var priceLabel: UILabel!
    
    var event: EventModel!
    var delegate: LWContainerViewHeightDelegate?
    
    static let storyboardName: String = "Event"
    static func prepareViewController() -> EventDescriptionTableViewController? {
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EventDescriptionTableViewController") as? EventDescriptionTableViewController
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.white
        tableView.separatorColor = UIColor.lightestGreen
        if let event = event {
            descriptionLabel.text = event.description
            var price = EventConstant.Label.free
            if event.price > 0 {
                price = String(format: EventConstant.Label.price, arguments: [event.price.currencyString()])
            }
            priceLabel.text = price
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        delegate?.didChange(height: tableView.contentSize.height)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        delegate?.didChange(height: tableView.contentSize.height)
    }
}

// MARK: - TableView
extension EventDescriptionTableViewController {
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}
