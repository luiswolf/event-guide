//
//  EventLocationViewController.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import MapKit

class EventLocationViewController: LWViewController {

    @IBOutlet weak var contentView: UIView! {
        didSet {
            contentView?.layer.cornerRadius = 8.0
            contentView?.layer.masksToBounds = true
            let divisor = UIScreen.main.scale > 2 ? 2 : UIScreen.main.scale
            contentView?.layer.borderColor = UIColor.lightGreen.cgColor
            contentView?.layer.borderWidth = 1.0 / divisor
        }
    }
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    
    var event: EventModel!
    var delegate: LWContainerViewHeightDelegate?
    
    fileprivate let bottomMargin: CGFloat = 32.0
    fileprivate let initialRegionRadius: CLLocationDistance = 1000
    
    static let storyboardName: String = "Event"
    static func prepareViewController() -> EventLocationViewController? {
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EventLocationViewController") as? EventLocationViewController
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        delegate?.didChange(height: contentView.frame.height + bottomMargin)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        delegate?.didChange(height: contentView.frame.height + bottomMargin)
    }
    
}

// MARK: - Actions
extension EventLocationViewController {
    @IBAction func openMaps(_ sender: UIButton) {
        guard let coordinate = event.getCoordinate() else { return }
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        destination.name = event.title
        MKMapItem.openMaps(with: [destination], launchOptions: nil)
    }
}

// MARK: - Helper
extension EventLocationViewController {
    func configureView() {
        guard let coordinate = event.getCoordinate() else { return }
        startLoading()
        mapButton.setImage(mapButton.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
        mapButton.tintColor = UIColor.blue
        coordinate.getLocation { (address) in
            if let address = address {
                self.locationLabel.text = address.getFormattedString()
            }
            self.centerMap(atLocation: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude), radius: self.initialRegionRadius)
            self.stopLoading()
        }
    }
    func centerMap(atLocation location: CLLocation, radius: CLLocationDistance) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: radius, longitudinalMeters: radius)
        mapView.setRegion(coordinateRegion, animated: true)
        addAnotation(forEvent: event)
    }
    func addAnotation(forEvent event: EventModel) {
        let annotation = EventAnnotation(event: event)
        mapView.addAnnotation(annotation)
    }
}
