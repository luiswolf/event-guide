//
//  EventListTableViewController.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 27/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit

class EventListTableViewController: LWTableViewController {

    fileprivate let viewModel = EventViewModel()
    fileprivate enum Identifier: String {
        case event, goToDetail
    }
    var detailViewController: EventDetailViewController? = nil
    
    static let storyboardName: String = "Event"
    static func prepareSplitViewController() -> UISplitViewController? {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        let splitViewController = storyboard.instantiateViewController(withIdentifier: "EventSplitViewController") as? UISplitViewController
        return splitViewController
    }
    static func prepareNavigationController() -> LWNavigationController? {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "EventListNavigationController") as? LWNavigationController
        return navigationController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundColor = UIColor.lightestGray
        tableView.separatorColor = UIColor.lightestGreen
        tableView.estimatedRowHeight = 120.0
        tableView.rowHeight = UITableView.automaticDimension
        
        startLoading()
        viewModel.delegate = self
        viewModel.getList()

        if let split = splitViewController {
            split.preferredDisplayMode = .allVisible
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? EventDetailViewController
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let indexPath = tableView.indexPathForSelectedRow else { return }
        if let navigationController = segue.destination as? UINavigationController, let vc = navigationController.topViewController as? EventDetailViewController, let state = viewModel.state as? EventListState, let eventList = state.getData() {
            vc.event = eventList[indexPath.row]
            vc.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            vc.navigationItem.leftItemsSupplementBackButton = true
        }
    }
}

// MARK: - Actions
extension EventListTableViewController {
    @IBAction fileprivate func profileButtonPressed(_ sender: UIBarButtonItem) {
        guard let profile = ProfileTableViewController.prepareNavigationController() else {
            return
        }
        guard let vc = profile.topViewController as? ProfileTableViewController else {
            return
        }
        vc.canSkip = false
        vc.isModal = true
        present(profile, animated: true, completion: nil)
    }
}

// MARK: - TableView DataSource
extension EventListTableViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let state = viewModel.state as? EventListState, let eventList = state.getData() else { return 0 }
        return eventList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let state = viewModel.state as? EventListState, let eventList = state.getData() else { return UITableViewCell() }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.event.rawValue, for: indexPath) as? EventTableViewCell else { return UITableViewCell() }
        
        let event = eventList[indexPath.row]
        cell.configure(event)
        return cell
    }
}

// MARK: - LWResponseRequestProtocol
extension EventListTableViewController: LWResponseRequestProtocol {
    func didFinishRequest() {
        guard let state = viewModel.state as? EventListState else { return }
        guard state.isSuccess() else {
            infoAction()
            return
        }
        successAction()
    }
}

// MARK: - Helper
extension EventListTableViewController {
    final fileprivate func successAction() {
        tableView.reloadData()
        // selecting the first item on ipad
        if UIDevice.current.userInterfaceIdiom == .pad, let firstIndexPath = tableView.indexPathsForVisibleRows?.first {
            tableView.selectRow(at: firstIndexPath, animated: true, scrollPosition: .none)
            self.performSegue(withIdentifier: Identifier.goToDetail.rawValue, sender: nil)
        }
        stopLoading()
    }
    final fileprivate func infoAction() {
        guard let state = viewModel.state as? EventListState else { return }
        switch state {
        case .noConnection, .error:
            stopLoading()
            infoView.isOffline = !state.isOnline()
            infoView.delegate = self
            infoView.actionButton.isHidden = false
            showInfo()
        case .noData:
            stopLoading()
            infoView.title = InfoConstant.Title.noData
            infoView.message = EventConstant.Message.noData
            infoView.actionButton.isHidden = true
            infoView.iconImageView.image = UIImage(named: InfoConstant.Image.noData)
            infoView.delegate = self
            showInfo()
        default: ()
        }
    }
}

// MARK: - InfoView Delegate
extension EventListTableViewController: InfoViewDelegate {
    func didPressRetryButton() {
        hideInfo()
        startLoading()
        viewModel.getList()
    }
}
