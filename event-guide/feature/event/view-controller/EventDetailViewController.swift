//
//  EventDetailViewController.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 27/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit
import Segmentio

class EventDetailViewController: LWViewController, LWSplitViewControllerDetailProtocol {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var segmentioView: Segmentio!

    fileprivate enum EventSection: Int {
        case detail, location
    }
    
    var event: EventModel!
    var viewModel = EventViewModel()
    
    fileprivate var isCheckedIn: Bool = false {
        didSet {
            if isCheckedIn {
                confirmButton?.tintColor = UIColor.lightGreen
            } else {
                confirmButton?.tintColor = UIColor.blue
            }
        }
    }
    fileprivate lazy var imageHelper: ImageHelper = {
        let helper = ImageHelper()
        return helper
    }()
    fileprivate lazy var descriptionVC: EventDescriptionTableViewController? = {
        guard let description = EventDescriptionTableViewController.prepareViewController() else {
            return nil
        }
        description.event = event
        description.delegate = self
        return description
    }()
    fileprivate lazy var locationVC: EventLocationViewController? = {
        guard let location = EventLocationViewController.prepareViewController() else {
            return nil
        }
        location.event = event
        location.delegate = self
        return location
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureSegmentio()
        viewModel.delegate = self
    }
}

// MARK: - Actions
extension EventDetailViewController {
    func hasSelectedItem() -> Bool {
        return event != nil
    }
    func checkout() {
        isCheckedIn = false
    }
    func checkin() {
        startLoading()
        disableInteraction()
        viewModel.checkin(atEventId: event.id)
    }
    @IBAction fileprivate func shareButtonPressed(_ sender: UIBarButtonItem) {
        guard let image = imageView.image, let date = dateLabel.text else { return }
        let text = String(format: EventConstant.Message.share, arguments: [event.title, date])
        let shareItems: [Any] = [image, text]
        let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        var excludedActivityTypes: [UIActivity.ActivityType] = [.assignToContact, .print, .postToWeibo, .addToReadingList, .postToVimeo, .openInIBooks]
        if #available(iOS 11.0, *) {
            excludedActivityTypes.append(.markupAsPDF)
        }
        activityViewController.excludedActivityTypes = excludedActivityTypes
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction fileprivate func confirmButtonPressed(_ sender: UIButton) {
        if isCheckedIn {
            // desconfirmação logica
            MessageHelper.sharedInstance.show(message: EventConstant.Message.confirmCancel)
            checkout()
            return
        }
        guard UserDefaultsHelper.User.getUser() != nil else {
            // user should register to confirm
            guard let profile = ProfileTableViewController.prepareNavigationController() else {
                return
            }
            guard let vc = profile.topViewController as? ProfileTableViewController else {
                return
            }
            vc.canSkip = false
            vc.isModal = true
            vc.delegate = self
            present(profile, animated: true, completion: nil)
            return
        }
        checkin()
    }
}
    
// MARK: - Helper
extension EventDetailViewController {
    func configureSegmentio() {
        var content = [SegmentioItem]()
        let descriptionItem = SegmentioItem(
            title: EventConstant.Label.info.uppercased(), image: nil
        )
        content.append(descriptionItem)
        let locationItem = SegmentioItem(
            title: EventConstant.Label.location.uppercased(), image: nil
        )
        content.append(locationItem)
        
        let indicators = SegmentioIndicatorOptions(
            type: .bottom, ratio: 1, height: 3, color: UIColor.green
        )
        let horizontalSeparator = SegmentioHorizontalSeparatorOptions(
            type: SegmentioHorizontalSeparatorType.bottom,
            height: 0,
            color: .clear
        )
        let verticalSeparator = SegmentioVerticalSeparatorOptions(ratio: 1, color: .clear)
        
        let states = SegmentioStates(
            defaultState: SegmentioState(
                backgroundColor: .clear,
                titleFont: UIFont.regular,
                titleTextColor: .green
            ),
            selectedState: SegmentioState(
                backgroundColor: .clear,
                titleFont: UIFont.bold,
                titleTextColor: .green
            ),
            highlightedState: SegmentioState(
                backgroundColor: .clear,
                titleFont: UIFont.regular,
                titleTextColor: .green
            )
        )
        
        let options = SegmentioOptions(
            backgroundColor: .white,
            segmentPosition: SegmentioPosition.fixed(maxVisibleItems: 2),
            scrollEnabled: true,
            indicatorOptions: indicators,
            horizontalSeparatorOptions: horizontalSeparator,
            verticalSeparatorOptions: verticalSeparator,
            labelTextAlignment: .center,
            segmentStates: states
        )
        segmentioView.selectedSegmentioIndex = 0
        segmentioView.valueDidChange = { segmentio, segmentIndex in
            guard let section = EventSection(rawValue: segmentIndex) else { return }
            self.remove(child: self.descriptionVC)
            self.remove(child: self.locationVC)
            switch section {
                case .detail:
                    self.add(child: self.descriptionVC, toContainer: self.containerView)
                case .location:
                    self.add(child: self.locationVC, toContainer: self.containerView)
            }
        }
        segmentioView.setup(
            content: content,
            style: SegmentioStyle.onlyLabel,
            options: options
        )
    }
    func configureView() {
        guard event != nil else {
            shareButton.isEnabled = false
            infoView.title = InfoConstant.Title.noData
            infoView.message = EventConstant.Message.noSelected
            infoView.actionButton.isHidden = true
            infoView.iconImageView.image = UIImage(named: InfoConstant.Image.noData)
            showInfo()
            return
        }
        
        confirmButton.setImage(confirmButton.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
        isCheckedIn = false
        
        titleLabel.text = event.title
        dateLabel.text = DateHelper.sharedInstance.getFormattedFullDate(from: event.date).capitalizingFirstLetter()
        calendarView.date = event.date
        calendarView.dropShadow()
        add(child: descriptionVC, toContainer: containerView)
        
        if let imageUrl = event.imageUrl {
            imageHelper.getImage(withPath: imageUrl) { [weak self] image in
                guard let image = image else { return }
                self?.imageView.image = image
            }
        }
    }
    func enableInteraction() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationItem.hidesBackButton = false
        shareButton.isEnabled = true
    }
    func disableInteraction() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationItem.hidesBackButton = true
        shareButton.isEnabled = false
    }
}

// MARK: - ContainerViewHeight Delegate
extension EventDetailViewController: LWContainerViewHeightDelegate {
    func didChange(height: CGFloat) {
        contentViewHeight.constant = height
    }
}

extension EventDetailViewController: LWResponseRequestProtocol {
    func didFinishRequest() {
        stopLoading()
        enableInteraction()
        guard let state = viewModel.state as? EventCheckinState, state.isSuccess() else {
            isCheckedIn = false
            MessageHelper.sharedInstance.show(message: EventConstant.Message.confirmError)
            return
        }
        isCheckedIn = true
        MessageHelper.sharedInstance.show(message: EventConstant.Message.confirmSuccess)
    }
}

extension EventDetailViewController: ProfileDelegate {
    func didFinishRegister() {
        checkin()
    }
    func didCloseRegister() {
        MessageHelper.sharedInstance.show(message: EventConstant.Message.confirmDismiss)
    }
}
