//
//  EventConstant.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 28/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

/// Constants for the Event package
enum EventConstant {
    
    /// Base API URL
    private static let baseApi = "http://5b840ba5db24a100142dcd8c.mockapi.io/api/"
    
    /// List of endpoints used in the networking requests
    enum Endpoint {
        case event(id: Int?), checkin
        
        private static let listUrl = EventConstant.baseApi + "events"
        private static let detailUrl = EventConstant.Endpoint.listUrl + "/{id}"
        private static let checkinUrl = EventConstant.baseApi + "checkin"
        
        static func event() -> Endpoint { return .event(id: nil) }
        
        /// Returns an endpoint URL string
        func getUrl() -> String {
            switch self {
                case let .event(id: id):
                    guard let id = id else { return Endpoint.listUrl }
                    return Endpoint.detailUrl.replacingOccurrences(of: "{id}", with: String(id))
                case .checkin:
                    return Endpoint.checkinUrl
            }
        }
    }
    
    /// List of labels used in Event package
    enum Label {
        static let free = "Entrada gratuito"
        static let info = "Informações"
        static let description = "Descrição"
        static let location = "Local"
        static let price = "Valor do ingresso: %@"
    }
    
    /// List of images used in Event package
    enum Image {
        static let defaultEvent = "im-event-default-image"
    }
    
    /// List of messages used in Event package
    enum Message {
        static let share = "%@\n%@"
        static let confirmDismiss = "Sua presença no evento não foi confirmada."
        static let confirmSuccess = "Sua presença foi confirmada com sucesso."
        static let confirmError = "Não foi possível confirmar sua presença no evento. Tente novamente mais tarde."
        static let confirmCancel = "Sua presença foi desconfirmada com sucesso."
        static let userError = "Não foi possível identificar usuário para confirmação de presença no evento."
        static let noData = "Parece que não há eventos disponíveis no momento."
        static let noSelected = "Parece que não há evento selecionado para ser exibido."
    }
}
