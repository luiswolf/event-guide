//
//  PersonState.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

enum PersonState {
    case exists(user: PersonModel)
    case noUser
    case validation(message: String)
    case saved(user: PersonModel)
    
    func isSuccess() -> Bool {
        switch self {
        case .exists, .saved:
            return true
        default:
            return false
        }
    }
    
    func getData() -> PersonModel? {
        switch self {
        case .exists(user: let data), .saved(user: let data):
            return data
        default:
            return nil
        }
    }
}
