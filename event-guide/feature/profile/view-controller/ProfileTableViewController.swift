//
//  ProfileTableViewController.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import UIKit
protocol ProfileDelegate {
    func didSkipRegister()
    func didCloseRegister()
    func didFinishRegister()
}
extension ProfileDelegate {
    func didSkipRegister() {}
    func didCloseRegister() {}
}

class ProfileTableViewController: LWTableViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton! {
        didSet {
            registerButton?.titleLabel?.font = UIFont.regular.withSize(16.0)
        }
    }
    
    fileprivate lazy var viewModel: ProfileViewModel = {
        let vm = ProfileViewModel()
        vm.delegate = self
        return vm
    }()
    
    fileprivate var closeButton: UIBarButtonItem?
    fileprivate var skipButton: UIBarButtonItem?
    
    var isModal: Bool = true
    var canSkip: Bool = true
    var delegate: ProfileDelegate?
    
    static let storyboardName: String = "Profile"
    static func prepareNavigationController() -> LWNavigationController? {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "ProfileNavigationController") as? LWNavigationController
        return navigationController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isModal {
            closeButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(close))
            navigationItem.leftBarButtonItem = closeButton
        }
        
        if canSkip {
            skipButton = UIBarButtonItem(title: ProfileConstant.Label.skip, style: UIBarButtonItem.Style.plain, target: self, action: #selector(skip))
            navigationItem.rightBarButtonItem = skipButton
        } else {
            if UserDefaultsHelper.User.getUser() != nil {
                let removeButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(remove))
                navigationItem.rightBarButtonItem = removeButton
            }
        }
        
        nameTextField.clearButtonMode = .whileEditing
        nameTextField.font = UIFont.regular.withSize(15.0)
        nameTextField.textColor = UIColor.blue
        nameTextField.autocapitalizationType = .words
        nameTextField.autocorrectionType = .no
        nameTextField.returnKeyType = .next
        nameTextField.delegate = self
        
        emailTextField.keyboardType = .emailAddress
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.autocorrectionType = .no
        emailTextField.font = UIFont.regular.withSize(15.0)
        emailTextField.textColor = UIColor.blue
        emailTextField.returnKeyType = .done
        emailTextField.delegate = self
        
        tableView.backgroundColor = UIColor.lightestGray
        tableView.separatorColor = UIColor.lightestGreen
        tableView.estimatedRowHeight = 120.0
        tableView.rowHeight = UITableView.automaticDimension
        
        viewModel.getData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nameTextField.becomeFirstResponder()
    }
}

// MARK: - UITableView DataSource
extension ProfileTableViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
}

// MARK: - Actions
extension ProfileTableViewController {
    @objc
    fileprivate func remove() {
        UserDefaultsHelper.User.clearUserData()
        UserDefaultsHelper.Application.clear()
        MessageHelper.sharedInstance.show(message: ProfileConstant.Message.clearData)
        viewModel.getData()
        view.endEditing(true)
    }
    @objc
    fileprivate func close() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
        delegate?.didCloseRegister()
    }
    @objc
    fileprivate func skip() {
        view.endEditing(true)
        delegate?.didSkipRegister()
    }
    @objc
    fileprivate func register() {
        view.endEditing(true)
        let person = PersonModel()
        person.name = nameTextField.text ?? String.init()
        person.email = emailTextField.text
        viewModel.saveData(person)
    }
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        register()
    }
}

// MARK: UITextField Delegate
extension ProfileTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text ?? String.init()
        
        if textField == nameTextField {
            guard !text.isEmpty else {
                textField.resignFirstResponder()
                MessageHelper.sharedInstance.show(message: ProfileConstant.Message.nameIsEmpty)
                return false
            }
            textField.resignFirstResponder()
            emailTextField.becomeFirstResponder()
        }
        if textField == emailTextField {
            guard !text.isEmpty else {
                textField.resignFirstResponder()
                MessageHelper.sharedInstance.show(message: ProfileConstant.Message.emailIsEmpty)
                return false
            }
            guard text.isValidEmail() else {
                textField.resignFirstResponder()
                MessageHelper.sharedInstance.show(message: ProfileConstant.Message.emailIsInvalid)
                return false
            }
            textField.resignFirstResponder()
            register()
        }
        return true
    }
}

// MARK: - LWResponseRequestProtocol
extension ProfileTableViewController: LWResponseRequestProtocol {
    func didFinishRequest() {
        guard let state = viewModel.state else { return }
        switch state {
            case .noUser:
                nameTextField.text = String.init()
                emailTextField.text = String.init()
            case .exists(user: let user):
                nameTextField.text = user.name
                emailTextField.text = user.email
            case .validation(message: let message):
                MessageHelper.sharedInstance.show(message: message)
            case .saved:
                delegate?.didFinishRegister()
                if isModal {
                    dismiss(animated: true, completion: nil)
                }
        }
    }
}
