//
//  ProfileViewModel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

class ProfileViewModel: NSObject {

    var person: PersonModel?
    
    weak var delegate: LWResponseRequestProtocol?
    
    fileprivate(set) var state: PersonState? {
        didSet {
            delegate?.didFinishRequest()
        }
    }
    
    func getData() {
        if let user = UserDefaultsHelper.User.getUser() {
            state = .exists(user: user)
            return
        }
        state = .noUser
    }
    
    func saveData(_ person: PersonModel) {
        guard person.isValid() else {
            state = .validation(message: ProfileConstant.Message.formInvalid)
            return
        }
        
        UserDefaultsHelper.User.save(user: person)
        state = .saved(user: person)
    }
    
}
