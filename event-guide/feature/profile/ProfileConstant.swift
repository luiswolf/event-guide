//
//  ProfileConstant.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation

/// Constants for the Profile package
enum ProfileConstant {
    /// List of messages used in Profile package
    enum Message {
        static let nameIsEmpty = "Por favor, informe seu nome."
        static let emailIsEmpty = "Por favor, informe seu e-mail."
        static let emailIsInvalid = "O e-mail informado é inválido. Por favor verifique."
        static let formInvalid = "Dados incompletos ou inválidos. Por favor verifique."
        static let clearData = "Dados do aplicativo removidos com sucesso."
    }
    
    /// List of labels used in Profile package
    enum Label {
        static let skip = "Pular"
    }
}
