//
//  PersonModel.swift
//  event-guide
//
//  Created by Luis Emilio Dias Wolf on 29/09/19.
//  Copyright © 2019 Luis Wolf. All rights reserved.
//

import Foundation
import ObjectMapper

class PersonModel: Mappable {
    var id: Int = 0
    var name: String = String.init()
    var pictureUrl: String?
    var email: String?
    
    init() {}
    required init?(map: Map){}
    init(_ json: [String: String]) {
        self.name = json[UserDefaultsHelper.User.nameKey] ?? String.init()
        self.email = json[UserDefaultsHelper.User.emailKey]
    }
    
    func mapping(map: Map) {
        id <- (map["id"], StringIntegerTransform())
        name <- map["name"]
        pictureUrl <- map["picture"]
        email <- map["email"]
    }
    
    func isValid() -> Bool {
        guard let email = email, !email.isEmpty, email.isValidEmail(), !name.isEmpty else {
            return false
        }
        return true
    }
}
